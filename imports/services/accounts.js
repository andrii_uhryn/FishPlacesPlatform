import { Meteor } from 'meteor/meteor';
import { throwIt } from '/imports/lib/throwIt';

export class AuthService {
  static checkLoggedIn = () => {
    if (!Meteor.userId()) {
      throwIt(401, 'You are not logged in');
    }

    const user = Meteor.users.findOne({ _id: Meteor.userId() });

    return user || throwIt(401, 'You are not logged in');
  };
}
