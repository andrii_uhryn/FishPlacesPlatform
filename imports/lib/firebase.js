import firebase from 'firebase-admin';
import { Meteor } from 'meteor/meteor';

import Users from '/imports/api/users/users';

const sendPN = (tokens, notification) => {
  firebase
  .messaging()
  .sendToDevice(tokens, notification)
  .then(response => {
    // For each message check if there was an error.
    const tokensToRemove = [];

    response.results.forEach((result, index) => {
      const error = result.error;

      if (error) {
        console.error('Failure sending notification to', tokens[index], error);
        // Cleanup the tokens who are not registered anymore.
        if (error.code === 'messaging/invalid-registration-token' ||
         error.code === 'messaging/registration-token-not-registered') {
          tokensToRemove.push(tokens[index]);
        }
      }
    });

    if (tokensToRemove.length) {
      Users.update(
       { 'profile.firebase.token': { $in: tokensToRemove } },
       { $unset: { 'profile.firebase.token': '' } },
       { multi: true }
      );
    }

    console.log('Users with bad tokens: ', tokensToRemove.length);
    console.log('Users with correct tokens: ', tokens.length - tokensToRemove.length);
  });
};

export const sendPNToAllUsersExceptCurrent = (notification, user) => {
  const query = {
    'profile.firebase.token': { $exists: true }
  };

  if (user) {
    query._id = { $ne: user._id };
  }

  const users = Users.find(query).fetch();

  if (users.length) {
    users.forEach(u => {
      sendPN(
       [u.profile.firebase.token],
       {
         ...notification,
         notification: {
           ...notification.notification,
           title: `${user.profile.firstName} ${user.profile.lastName} ${Meteor.settings[u.profile.language || 'en']?.notifications?.[notification.data.type]}`
         }
       }
      );
    });
  } else {
    console.log('no users with tokens found');
  }
};

export const sendPNToUser = (notification, userIdToSend) => {
  if (!userIdToSend) {
    return false;
  }

  const query = {
    _id: userIdToSend,
    'profile.firebase.token': { $exists: true }
  };

  const user = Users.findOne(query);

  if (user) {
    sendPN(
     [user.profile.firebase.token],
     notification
    );
  } else {
    console.log('User does not have token');
  }
};
