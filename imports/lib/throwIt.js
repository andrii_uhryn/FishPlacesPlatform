export const throwIt = (error, message) => {
  throw new Meteor.Error(error, message)
}
