import { Meteor } from 'meteor/meteor';
import escape from 'escape-html';

const getData = (data = {}) => ({
    environment: Meteor.settings.environment,
    ...Meteor.settings[data.language],
    ...data,
});

const renderHtml = (tpl, data = {}) => {
    data = Object.keys(data).reduce((d, k) => {
        if (k.slice(0, 4) === 'html') {
            // Rename html prefixed keys e.g. htmlSignature -> signature
            d[k.slice(4, 5).toLowerCase() + k.slice(5)] = data[k];
        } else {
            // HTML escape values whose keys are not prefixed with "html"
            // Only assign and escape if not already exists in memo
            if (d[k] == null) d[k] = escape(data[k]);
        }
        return d;
    }, {});

    // Special "content" property is interpolated
    if (data.content) data.content = interpolate(data.content, data);
    return interpolate(tpl, data);
};

const renderText = (tpl, data = {}) => {
    // Special "content" property is interpolated
    if (data.content) data.content = interpolate(data.content, data);
    return interpolate(tpl, data);
};

const interpolate = (str, data) => {
    return str.replace(/{{([a-z]+)}}/ig, (match, key) => {
        return data[key] == null ? '' : data[key];
    });
};

// Templates are cached in memory on demand
const templates = {};

const getTemplate = (language, name, ext) => {
    const key = `emails/${language}/${name}.${ext}`;
    if (templates[key]) return templates[key];
    templates[key] = Assets.getText(key);
    return templates[key];
};

export const htmlTemplate = (name, language) => {
    const layoutTpl = getTemplate(language, 'layout', 'html');
    const tpl = getTemplate(language, name, 'html');

    return (data) => {
        data = getData(data);
        const content = renderHtml(tpl, data);
        return renderHtml(layoutTpl, { ...data, htmlContent: content });
    };
};

export const textTemplate = name => {
    const layoutTpl = getTemplate('layout', 'txt');
    const tpl = getTemplate(name, 'txt');

    return (data) => {
        data = getData(data);
        const content = renderText(tpl, data);
        return renderText(layoutTpl, { ...data, content });
    };
};
