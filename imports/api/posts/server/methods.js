import { Meteor } from 'meteor/meteor';
import { AuthService } from '/imports/services/accounts';

import Posts from '../posts';
import Users from '../../users/users';
import PostsService from './services';

import { throwIt } from '../../../lib/throwIt';

Meteor.methods({
  'posts/get/list'(body) {
    this.unblock();
    const user = AuthService.checkLoggedIn(true);

    return PostsService.getList(body, user);
  },

  'post/get/likes/list'(body) {
    this.unblock();
    const user = AuthService.checkLoggedIn(true);

    const post = Posts.findOne(body.post_id);

    if (!post) {
      throwIt('not-found', 'Post does not exist');
    }

    return PostsService.getLikesList(body, user, post);
  },

  'posts/get/list/byUser'(body) {
    this.unblock();
    let user = AuthService.checkLoggedIn();

    if (body.userId && body.userId !== user._id) {
      user = Users.findOne(body.userId);
    }

    if (!user) {
      throwIt('not-found', 'User does not exist');
    }

    return PostsService.getListByUser(body, user);
  },

  'posts/create'(body) {
    this.unblock();
    const user = AuthService.checkLoggedIn();

    return PostsService.createPost(body, user);
  },

  'posts/update'(body) {
    this.unblock();
    const user = AuthService.checkLoggedIn();
    const post = Posts.findOne(body._id);

    if (!post) {
      throwIt('not-found', 'Post does not exist');
    }

    return PostsService.updatePost(body, user, post);
  },

  'posts/updateLikes'(body) {
    this.unblock();
    const user = AuthService.checkLoggedIn();
    const post = Posts.findOne(body._id);

    if (!post) {
      throwIt('not-found', 'Post does not exist');
    }

    return PostsService.updatePostLikes(body, user, post);
  },

  'posts/inappropriate'(body) {
    this.unblock();
    const currentUser = AuthService.checkLoggedIn();
    const post = Posts.findOne(body.post?._id);

    if (!post) {
      throwIt('not-found', 'Post does not exist');
    }

    return PostsService.inappropriatePost(body, currentUser, post);
  },

  'posts/delete'(body) {
    this.unblock();
    AuthService.checkLoggedIn();

    if (!Posts.findOne(body._id)) {
      throwIt('not-found', 'Post does not exist');
    }

    return PostsService.deletePost(body);
  }
});
