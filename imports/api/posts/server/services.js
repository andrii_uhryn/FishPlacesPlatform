import cloudinary from 'cloudinary';
import { Meteor } from 'meteor/meteor';

import Posts from '../posts';
import Users from '../../users/users';
import Feedbacks from '../../feedbacks/feedbacks';
import { PostSchema } from './validations';
import { FeedbacksSchema } from '../../feedbacks/server/validations';

import { htmlTemplate } from '../../emails/server/template';
import { sendPNToUser, sendPNToAllUsersExceptCurrent } from '../../../lib/firebase';

const populateWithUser = (collection, user) => {
  if (user) {
    return collection.map(p => {
      p.createdBy.user = user;

      return p;
    });
  }

  const users = Users.find(
   {
     _id: {
       $in: collection.map(p => p.createdBy._id)
     }
   }
  ).fetch();

  return collection.map(p => {
    p.createdBy.user = users.find(u => u._id === p.createdBy._id);

    return p;
  });
};

const calculateDistanceInMeters = (lat1, lon1, lat2, lon2) => {
  const R = 6378.137; // Radius of earth in KM
  const dLat = (lat2 * Math.PI) / 180 - (lat1 * Math.PI) / 180;
  const dLon = (lon2 * Math.PI) / 180 - (lon1 * Math.PI) / 180;
  const a =
   Math.sin(dLat / 2) * Math.sin(dLat / 2) +
   Math.cos((lat1 * Math.PI) / 180) *
   Math.cos((lat2 * Math.PI) / 180) *
   Math.sin(dLon / 2) *
   Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;
  return d * 1000;
};

const getBoundingBox = (region) => [
  region.longitude - region.longitudeDelta / 2, // westLng - min lng
  region.latitude - region.latitudeDelta / 2, // southLat - min lat
  region.longitude + region.longitudeDelta / 2, // eastLng - max lng
  region.latitude + region.latitudeDelta / 2 // northLat - max lat
];

export const getRadius = (region) => {
  const coordinates = getBoundingBox(region);
  return calculateDistanceInMeters(...coordinates) / 2;
};

export default class PostsService {
  static getList = (body, user) => {
    const { sort, skip, limit, search, region } = body;
    const query = {};

    if (search) {
      const searchQuery = { $regex: search, $options: 'i' };

      query.$or = [
        {
          title: searchQuery
        },
        {
          description: searchQuery
        },
        {
          'location.label': searchQuery
        }
      ];
    }

    if (region) {
      query.location = {
        $near:
         {
           $geometry: {
             type: 'Point',
             coordinates: [
               region.longitude,
               region.latitude
             ]
           },
           $minDistance: 0,
           $maxDistance: getRadius(region)
         }
      };
    }

    const posts = Posts.find(
     query,
     {
       sort,
       skip,
       limit
     }
    ).fetch();

    Users.update(
     { _id: user._id },
     { $set: { 'profile.refresh.posts': false } },
     { multi: false }
    );

    return populateWithUser(posts);
  };

  static getListByUser = (body, user) => {
    const { sort, skip, limit } = body;

    const posts = Posts.find(
     {
       'createdBy._id': user._id
     },
     {
       sort,
       skip,
       limit
     }
    ).fetch();

    return populateWithUser(posts, user);
  };

  static getLikesList = (body, user, post) => {
    const { sort, skip, limit } = body;

    const likes = post.likes
    .sort((a, b) => {
      if (typeof sort.date !== 'undefined') {
        if (a.date > b.date) {
          return sort.date;
        } else {
          return -sort.date;
        }
      }

      return 0;
    })
    .slice(skip, skip ? skip * limit : limit);

    const users = Users.find(
     {
       _id: {
         $in: likes.map(p => p._id)
       }
     }
    ).fetch();

    return likes.map(like => {
      return {
        ...like,
        user: users.find(u => u._id === like._id),
      }
    });
  };

  static createPost = (body, user) => {
    const createdBy = {
      _id: user._id,
      date: new Date()
    };

    body.createdBy = createdBy;
    body.updatedBy = createdBy;

    PostSchema.validate(body);

    const postId = Posts.insert(body);
    const post = Posts.findOne(postId);

    Users.update(
     { _id: { $ne: user._id } },
     { $set: { 'profile.refresh.posts': true } },
     { multi: true }
    );

    if (!user.profile.hasFirstPost) {
      Users.update(
       { _id: user._id },
       { $set: { 'profile.hasFirstPost': true } },
       { multi: false }
      );
    }

    // notify all users that new post was added
    sendPNToAllUsersExceptCurrent(
     {
       data: {
         type: 'new_post'
       },
       notification: {}
     },
     user
    );

    return populateWithUser([post])[0];
  };

  static updatePost = async (body, user, post) => {
    body.updatedBy = {
      _id: user._id,
      date: new Date()
    };

    PostSchema.validate(body);

    if (post.image?.public_id && body.image?.public_id && post.image?.public_id !== body.image?.public_id) {
      await cloudinary.uploader.destroy(post.image?.public_id);
    }

    Posts.update(body._id, body);

    const res = Posts.findOne(body._id);

    return populateWithUser([res])[0];
  };

  static updatePostLikes = async (body, user, post) => {
    const data = {};

    if (post.likes?.find(l => l._id === user._id)) {
      data['$pull'] = { likes: { _id: user._id } };
    } else {
      data['$push'] = {
        likes: {
          _id: user._id,
          date: new Date()
        }
      };

      if (user._id !== post.createdBy._id) {
        const userToSend = Users.findOne(post.createdBy._id);

        sendPNToUser(
         {
           data: {
             type: 'post_liked',
             postId: post._id
           },
           notification: {
             title: `${user.profile.firstName} ${user.profile.lastName} ${Meteor.settings[userToSend.profile.language || 'en']?.notifications?.post_liked}`
           }
         },
         userToSend._id
        );
      }
    }

    Posts.update({ _id: body._id }, data, { multi: false });

    const res = Posts.findOne(body._id);

    return populateWithUser([res])[0];
  };

  static inappropriatePost = ({ reason }, user, post) => {
    const createdBy = {
      _id: user._id,
      date: new Date()
    };
    const feedback = {
      reason,
      createdBy,
      type: 'inappropriate.post',
      postId: post._id,
      updatedBy: createdBy
    };

    FeedbacksSchema.validate(feedback);

    Feedbacks.insert(feedback);

    const html = htmlTemplate('inappropriatePost', 'en');

    Email.send({
      to: Meteor.settings.en.email.from,
      from: Meteor.settings.en.email.from,
      html: html({ ...feedback, name: `${user.profile.firstName} ${user.profile.lastName}` }),
      subject: Meteor.settings.en.email.inappropriatePost.subject
    });

    return true;
  };

  static deletePost = async ({ _id, image }) => {
    await cloudinary.uploader.destroy(image.public_id);

    return Posts.remove({ _id });
  };
}
