import SimpleSchema from 'simpl-schema';

import { AvatarSchema, LocationSchema } from '../../users/server/validations';

export const CreatedBySchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    date: {
        type: Date,
        optional: true
    },
});

export const LikesSchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    date: {
        type: Date,
        optional: true
    },
});

export const PostSchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    title: {
        type: String,
        optional: true
    },
    bait: {
        type: String,
        optional: true
    },
    date: {
        type: Date,
        optional: true
    },
    likes: {
        type: Array,
        optional: true
    },
    'likes.$': {
      type: LikesSchema,
      optional: true
    },
    commentsCount: {
        type: Number,
        optional: true,
        defaultValue: 0,
    },
    location: {
        type: LocationSchema,
        optional: true
    },
    description: {
        type: String,
        optional: true
    },
    image: {
        type: AvatarSchema,
        optional: true,
    },
    public: {
        type: Boolean,
        optional: true
    },
    createdBy: {
        type: CreatedBySchema,
        optional: true
    },
    updatedBy: {
        type: CreatedBySchema,
        optional: true
    },
});
