import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';

const Posts = new Mongo.Collection('posts');

if (Meteor.isServer) {
  Posts._ensureIndex({ 'createdBy._id': 1 });
  Posts._ensureIndex({ 'updatedBy._id': 1 });
  Posts._ensureIndex({ 'location': '2dsphere' });
}

export default Posts;
