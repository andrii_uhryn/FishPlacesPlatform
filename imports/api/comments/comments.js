import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';

const Comments = new Mongo.Collection('comments');

if (Meteor.isServer) {
  Comments._ensureIndex({ 'createdBy._id': 1 });
  Comments._ensureIndex({ 'updatedBy._id': 1 });
}

export default Comments;
