import { Meteor } from 'meteor/meteor';
import { AuthService } from '/imports/services/accounts';

import Posts from '../../posts/posts';
import Comments from '../comments';
import CommentsService from './services';

import { throwIt } from '../../../lib/throwIt';

Meteor.methods({
  'comments/get/list'(body) {
    this.unblock();
    const user = AuthService.checkLoggedIn(true);

    return CommentsService.getList(body, user);
  },

  'comments/create'(body) {
    this.unblock();
    const user = AuthService.checkLoggedIn();

    const post = Posts.findOne(body.post_id);

    if (!post) {
      throwIt('not-found', 'Posts does not exist');
    }

    return CommentsService.createComment(body, user, post);
  },

  'comments/update'(body) {
    this.unblock();
    const user = AuthService.checkLoggedIn();
    const comment = Comments.findOne(body._id);

    if (!comment) {
      throwIt('not-found', 'Comment does not exist');
    }

    return CommentsService.updateComment(body, user, comment);
  },

  'comments/delete'(body) {
    this.unblock();
    AuthService.checkLoggedIn();

    const comment = Comments.findOne(body._id);

    if (!comment) {
      throwIt('not-found', 'Comment does not exist');
    }

    return CommentsService.deleteComment(body, comment);
  }
});
