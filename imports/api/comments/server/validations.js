import SimpleSchema from 'simpl-schema';

export const CreatedBySchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    date: {
        type: Date,
        optional: true
    },
});

export const CommentSchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    post_id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    text: {
        type: String,
        optional: true
    },
    createdBy: {
        type: CreatedBySchema,
        optional: true
    },
    updatedBy: {
        type: CreatedBySchema,
        optional: true
    },
});
