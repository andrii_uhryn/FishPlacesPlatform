import { Meteor } from 'meteor/meteor';

import Users from '../../users/users';
import Posts from '../../posts/posts';
import Comments from '../comments';
import { CommentSchema } from './validations';

import { sendPNToUser } from '../../../lib/firebase';

const populateWithUser = (collection, user) => {
  if (user) {
    return collection.map(p => {
      p.createdBy.user = user;

      return p;
    });
  }

  const users = Users.find(
   {
     _id: {
       $in: collection.map(p => p.createdBy._id)
     }
   }
  ).fetch();

  return collection.map(p => {
    p.createdBy.user = users.find(u => u._id === p.createdBy._id);

    return p;
  });
};

export default class CommentsService {
  static getList = (body) => {
    const { sort, skip, limit, search, post_id } = body;
    const query = {
      post_id,
    };

    if (search) {
      const searchQuery = { $regex: search, $options: 'i' };

      query.$or = [
        {
          title: searchQuery
        },
        {
          description: searchQuery
        },
      ];
    }

    const posts = Comments.find(
     query,
     {
       sort,
       skip,
       limit
     }
    ).fetch();

    return populateWithUser(posts);
  };

  static createComment = (body, user, post) => {
    const createdBy = {
      _id: user._id,
      date: new Date()
    };

    body.createdBy = createdBy;
    body.updatedBy = createdBy;

    CommentSchema.validate(body);

    const commentId = Comments.insert(body);
    const res = Comments.findOne(commentId);

    Posts.update(
     { _id: res.post_id },
     { $inc: { 'commentsCount': 1 } },
     { multi: false }
    );

    if (user._id !== post.createdBy._id) {
      const userToSend = Users.findOne(post.createdBy._id);

      sendPNToUser(
       {
         data: {
           type: 'new_comment',
           postId: post._id
         },
         notification: {
           title: `${user.profile.firstName} ${user.profile.lastName} ${Meteor.settings[userToSend.profile.language || 'en']?.notifications?.new_comment}`
         }
       },
       userToSend._id
      );
    }

    return populateWithUser([res])[0];
  };

  static updateComment = async (body, user) => {
    body.updatedBy = {
      _id: user._id,
      date: new Date()
    };

    CommentSchema.validate(body);

    Comments.update(body._id, body);

    const res = Comments.findOne(body._id);

    return populateWithUser([res])[0];
  };

  static deleteComment = async ({ _id }, comment) => {
    Posts.update(
     { _id: comment.post_id },
     { $inc: { 'commentsCount': -1 } },
     { multi: false }
    );

    return Comments.remove({ _id });
  };
}
