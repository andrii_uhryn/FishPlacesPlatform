import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

import ROLES from '/constants/roles';

import { htmlTemplate } from '../../emails/server/template';

import {
  SignUpSchema,
  VerifyAccountSchema,
  ChangePasswordSchema,
  ChangePasswordCodeSchema,
  SendVerificationCodeSchema
} from './validations';

export const signUp = new ValidatedMethod({
  name: 'signUp',

  validate: SignUpSchema.validator(),

  run({ email, password, profile }) {
    const user = Meteor.users.findOne({ 'emails.0.address': email });

    if (user) {
      throw new Meteor.Error(403, 'An account with this email already exists.');
    }

    profile.registered = true;

    const userId = Accounts.createUser({ email, password, profile });

    Meteor.users.update({ _id: userId }, { $set: { roles: [ROLES.USER] } });

    return true;
  }
});

export const verifyAccount = new ValidatedMethod({
  name: 'verifyAccount',

  validate: VerifyAccountSchema.validator(),

  run({ pin, email }) {
    const user = Meteor.users.findOne({ 'emails.0.address': email });

    if (!user) {
      throw new Meteor.Error(400, 'User not found');
    } else if (user.services.email.verificationTokens.length === 0) {
      throw new Meteor.Error(400, 'Send verification code first');
    }

    const valid = user.services.email.verificationTokens.filter(item => +item.token === pin);

    if (!valid.length) {
      throw new Meteor.Error(400, 'Wrong code');
    }

    Meteor.users.update(
     { _id: user._id },
     {
       $set: {
         'services.email.verificationTokens': [],
         'emails.0.verified': true
       }
     },
     error => {
       if (error) {
         throw error;
       }

       const html = htmlTemplate('welcome', user.profile.language);

       const data = {
         name: `${user.profile.firstName} ${user.profile.lastName}`,
         language: user.profile.language
       };

       Email.send({
         to: `${user.profile.firstName} ${user.profile.lastName} <${email}>`,
         from: Meteor.settings[user.profile.language].email.from,
         html: html(data),
         subject: Meteor.settings[user.profile.language].email.welcome.subject
       });
     }
    );

    return true;
  }
});

export const verifyChangePassword = new ValidatedMethod({
  name: 'verifyChangePassword',

  validate: ChangePasswordSchema.validator(),

  run({ pin, email, newPassword }) {
    const user = Meteor.users.findOne({ 'emails.0.address': email });

    if (!user) {
      throw new Meteor.Error(400, 'User not found');
    } else if (user.services.changePassword.verificationTokens.length === 0) {
      throw new Meteor.Error(400, 'Send verification code first');
    }

    const valid = user.services.changePassword.verificationTokens.filter(item => +item.token === pin);

    if (!valid.length) {
      throw new Meteor.Error(400, 'Wrong code');
    }

    Meteor.users.update(
     { _id: user._id },
     {
       $set: {
         'services.changePassword.verificationTokens': []
       }
     },
     error => {
       if (error) {
         throw error;
       }

       Accounts.setPassword(user._id, newPassword);

       const html = htmlTemplate('changePasswordSuccess', user.profile.language);

       const data = {
         name: `${user.profile.firstName} ${user.profile.lastName}`,
         language: user.profile.language
       };

       Email.send({
         to: `${user.profile.firstName} ${user.profile.lastName} <${email}>`,
         from: Meteor.settings[user.profile.language].email.from,
         html: html(data),
         subject: Meteor.settings[user.profile.language].email.changePasswordSuccess.subject
       });
     }
    );

    return true;
  }
});

export const verifyChangePasswordCode = new ValidatedMethod({
  name: 'verifyChangePasswordCode',

  validate: ChangePasswordCodeSchema.validator(),

  run({ pin, email }) {
    const user = Meteor.users.findOne({ 'emails.0.address': email });

    if (!user) {
      throw new Meteor.Error(400, 'User not found');
    } else if (user.services.changePassword.verificationTokens.length === 0) {
      throw new Meteor.Error(400, 'Send verification code first');
    }

    const valid = user.services.changePassword.verificationTokens.filter(item => +item.token === pin);

    return !!valid.length;
  }
});

export const sendVerificationCode = new ValidatedMethod({
  name: 'sendVerificationCode',

  validate: SendVerificationCodeSchema.validator(),

  run({ email }) {
    const user = Meteor.users.findOne({ 'emails.0.address': email });

    if (!user) {
      throw new Meteor.Error(400, 'User not found');
    } else if (user.emails[0].verified) {
      throw new Meteor.Error(400, 'Email already verified');
    }

    const tokenRecord = {
      when: new Date(),
      token: Math.random().toString().substring(2, Meteor.settings.general.verify_pin_length + 2),
      address: email
    };

    Meteor.users.update(
     { _id: user._id },
     {
       $push: {
         'services.email.verificationTokens': tokenRecord
       }
     },
     error => {
       if (error) {
         throw error;
       }

       const html = htmlTemplate('sendVerificationCode', user.profile.language);

       const data = {
         pin: tokenRecord.token,
         name: `${user.profile.firstName} ${user.profile.lastName}`,
         language: user.profile.language
       };

       Email.send({
         to: `${user.profile.firstName} ${user.profile.lastName} <${email}>`,
         from: Meteor.settings[user.profile.language].email.from,
         html: html(data),
         subject: Meteor.settings[user.profile.language].email.sendVerificationCode.subject
       });
     }
    );
    return true;
  }
});

export const sendChangePasswordVerification = new ValidatedMethod({
  name: 'sendChangePasswordVerification',

  validate: SendVerificationCodeSchema.validator(),

  run({ email }) {
    const user = Meteor.users.findOne({ 'emails.0.address': email });

    if (!user) {
      throw new Meteor.Error(400, 'User not found');
    }

    const tokenRecord = {
      when: new Date(),
      token: Math.random().toString().substring(2, Meteor.settings.general.verify_pin_length + 2),
      address: email
    };

    Meteor.users.update(
     { _id: user._id },
     {
       $push: {
         'services.changePassword.verificationTokens': tokenRecord
       }
     },
     error => {
       if (error) {
         throw error;
       }

       const html = htmlTemplate('sendChangePasswordVerificationCode', user.profile.language);

       const data = {
         pin: tokenRecord.token,
         name: `${user.profile.firstName} ${user.profile.lastName}`,
         language: user.profile.language
       };

       Email.send({
         to: `${user.profile.firstName} ${user.profile.lastName} <${email}>`,
         from: Meteor.settings[user.profile.language].email.from,
         html: html(data),
         subject: Meteor.settings[user.profile.language].email.sendChangePasswordVerificationCode.subject
       });
     }
    );
    return true;
  }
});
