import SimpleSchema from 'simpl-schema';
import { ProfileSchema } from '../../users/server/validations';

export const SignUpSchema = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
    },
    password: {
        type: String,
    },
    profile: {
        type: ProfileSchema,
    },
});

export const VerifyAccountSchema = new SimpleSchema({
    pin: {
        type: Number,
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
    },
});

export const ChangePasswordSchema = new SimpleSchema({
    pin: {
        type: Number,
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
    },
    newPassword: {
        type: String,
    },
});

export const ChangePasswordCodeSchema = new SimpleSchema({
    pin: {
        type: Number,
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
    },
});

export const SendVerificationCodeSchema = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
    },
});
