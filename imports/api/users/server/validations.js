import SimpleSchema from 'simpl-schema';

export const AvatarSchema = new SimpleSchema({
  public_id: {
    type: String,
    optional: true
  },
  version: {
    type: Number,
    optional: true
  },
  signature: {
    type: String,
    optional: true
  },
  format: {
    type: String,
    optional: true
  },
  resource_type: {
    type: String,
    optional: true
  },
  created_at: {
    type: String,
    optional: true
  },
  uri: {
    type: String,
    optional: true
  },
  url: {
    type: String,
    optional: true
  },
  secure_url: {
    type: String,
    optional: true
  },
  delete_token: {
    type: String,
    optional: true
  }
});

export const CheckIfUserExistsSchema = new SimpleSchema({
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  }
});

export const RefreshSchema = new SimpleSchema({
  posts: {
    type: Boolean,
    optional: true
  }
});

export const FirebaseSchema = new SimpleSchema({
  token: {
    type: String,
    optional: true
  }
});

export const LocationSchema = new SimpleSchema({
  id: {
    type: String,
    optional: true
  },
  label: {
    type: String,
    optional: true
  },
  type: {
    type: String,
    allowedValues: ['Point']
  },
  coordinates: {
    type: Array,
    minCount: 2,
    maxCount: 2
  },
  'coordinates.$': {
    type: Number
  }
});

export const ProfileSchema = new SimpleSchema({
  firstName: {
    type: String,
    optional: true
  },
  lastName: {
    type: String,
    optional: true
  },
  language: {
    type: String,
    optional: true
  },
  hasFirstPost: {
    type: Boolean,
    optional: true
  },
  avatar: {
    type: AvatarSchema,
    optional: true
  },
  birthDate: {
    type: Date,
    optional: true
  },
  topCatch: {
    type: String,
    optional: true
  },
  location: {
    type: String,
    optional: true
  },
  refresh: {
    type: RefreshSchema,
    optional: true
  },
  firebase: {
    type: FirebaseSchema,
    optional: true
  }
});
