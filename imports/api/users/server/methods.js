import cloudinary from 'cloudinary';
import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

import { AuthService } from '/imports/services/accounts';

import Users from '../users';
import UserService from './services';
import { ProfileSchema, CheckIfUserExistsSchema } from './validations';

export const updateProfile = new ValidatedMethod({
  name: 'updateProfile',

  validate: ProfileSchema.validator(),

  run(profile) {
    const user = Meteor.users.findOne({ '_id': this.userId });

    if (!user) {
      throw new Meteor.Error(403, 'User not found');
    }

    const keys = Object.keys(profile);

    const dataToUpdate = {};

    keys.forEach(key => {
      dataToUpdate[`profile.${key}`] = profile[key];
    });

    if (user.profile?.avatar?.public_id && dataToUpdate.profile?.avatar?.public_id && user.profile?.avatar?.public_id !== dataToUpdate.profile?.avatar?.public_id) {
      cloudinary.uploader.destroy(user.profile.avatar?.public_id);
    }

    Meteor.users.update({ _id: this.userId }, { $set: dataToUpdate });

    return Meteor.users.findOne({ '_id': this.userId });
  }
});


export const checkIfUserExists = new ValidatedMethod({
  name: 'checkIfUserExists',

  validate: CheckIfUserExistsSchema.validator(),

  run({ email }) {
    return !!Meteor.users.findOne({ 'emails.0.address': email });
  }
});

import { throwIt } from '../../../lib/throwIt';

Meteor.methods({
  'users/inappropriate'(body) {
    this.unblock();

    const currentUser = AuthService.checkLoggedIn();
    const user = Users.findOne(body.user?._id);

    if (!user) {
      throwIt('not-found', 'User does not exist');
    }

    return UserService.inappropriateUser(body, currentUser, user);
  }
});

