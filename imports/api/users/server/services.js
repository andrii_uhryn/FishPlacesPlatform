import { Meteor } from 'meteor/meteor';

import Feedbacks from '../../feedbacks/feedbacks';
import { FeedbacksSchema } from '../../feedbacks/server/validations';

import { htmlTemplate } from '../../emails/server/template';

export default class UsersService {
  static inappropriateUser = ({ reason }, currentUser, user) => {
    const createdBy = {
      _id: currentUser._id,
      date: new Date()
    };

    const feedback = {
      reason,
      createdBy,
      type: 'inappropriate.user',
      userId: user._id,
      updatedBy: createdBy
    };

    FeedbacksSchema.validate(feedback);

    Feedbacks.insert(feedback);

    const html = htmlTemplate('inappropriateUser', 'en');

    Email.send({
      to: Meteor.settings.en.email.from,
      from: Meteor.settings.en.email.from,
      html: html({ ...feedback, name: `${currentUser.profile.firstName} ${currentUser.profile.lastName}` }),
      subject: Meteor.settings.en.email.inappropriateUser.subject
    });

    return true;
  };
}
