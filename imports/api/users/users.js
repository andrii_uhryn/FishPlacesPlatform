import { Meteor } from 'meteor/meteor';

import everything from '/imports/lib/everything';

const Users = Meteor.users;

Users.deny(everything);

export default Users;
