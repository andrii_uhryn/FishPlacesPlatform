import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';

const Feedbacks = new Mongo.Collection('feedbacks');

if (Meteor.isServer) {
  Feedbacks._ensureIndex({ 'createdBy._id': 1 });
  Feedbacks._ensureIndex({ 'updatedBy._id': 1 });
}

export default Feedbacks;
