import Feedbacks from '../../feedbacks/feedbacks';
import { FeedbacksSchema } from './validations';
import { htmlTemplate } from '../../emails/server/template';
import { Meteor } from 'meteor/meteor';

export default class FeedbacksService {
  static create = (body) => {
    const createdBy = {
      date: new Date()
    };

    body.createdBy = createdBy;
    body.updatedBy = createdBy;

    FeedbacksSchema.validate(body);

    const postId = Feedbacks.insert(body);

    const html = htmlTemplate('contactUs', 'en');

    const extraData = {
      name: body.name,
      email: body.email,
      reason: body.reason
    };

    Email.send({
      to: Meteor.settings.en.email.from,
      from: `${body.name} <${body.email}>`,
      html: html(extraData),
      subject: Meteor.settings.en.email.contactUs.subject
    });

    return { postId };
  };
}
