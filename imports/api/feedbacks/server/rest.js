import FeedbacksService from './services';

export const handleCreate = (req, res) => {
  const { body } = req;

  if (!body.name || !body.email || !body.reason) {
    res.writeHead(400);
    res.send();
  } else {
    FeedbacksService.create(req.body);

    res.writeHead(200);
    res.send();
  }
};
