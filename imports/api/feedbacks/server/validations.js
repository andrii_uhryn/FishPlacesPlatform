import SimpleSchema from 'simpl-schema';

export const CreatedBySchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    date: {
        type: Date,
        optional: true
    },
});

export const FeedbacksSchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    email: {
        type: String,
        optional: true
    },
    name: {
        type: String,
        optional: true
    },
    postId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    userId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    reason: {
        type: String,
        optional: true
    },
    type: {
        type: String,
        optional: true
    },
    createdBy: {
        type: CreatedBySchema,
        optional: true
    },
    updatedBy: {
        type: CreatedBySchema,
        optional: true
    },
});
