import { Meteor } from 'meteor/meteor';
import FeedbacksService from './services';

Meteor.methods({
  'feedbacks/create'(body) {
    this.unblock();

    return FeedbacksService.create(body);
  }
});
