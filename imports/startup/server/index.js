import _ from 'lodash';
import cors from 'cors';
import express from 'express';
import { HTTP } from 'meteor/http';
import firebase from 'firebase-admin';
import bodyParser from 'body-parser';
import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';
import appleSignIn from 'apple-signin-auth';
import SimpleSchema from 'simpl-schema';
import { Accounts } from 'meteor/accounts-base';
import { Cloudinary } from 'meteor/socialize:cloudinary';

import '/imports/api/auth/server/methods';
import '/imports/api/users/server/methods';
import '/imports/api/posts/server/methods';
import '/imports/api/comments/server/methods';
import '/imports/api/feedbacks/server/methods';

import * as FeedbackHandlers from '/imports/api/feedbacks/server/rest';

import { htmlTemplate } from '../../api/emails/server/template';

const app = express();

Meteor.startup(() => {
  if (Meteor.settings.email_url) {
    process.env.MAIL_URL = Meteor.settings.email_url;
  }


  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.set('trust proxy', true);
  app.set('trust proxy', 'loopback');

  app.post('/feedbacks/create', Meteor.bindEnvironment(FeedbackHandlers.handleCreate));

  WebApp.connectHandlers.use(app);

  Accounts.validateLoginAttempt(options => {
    if (!options.allowed) {
      return false;
    }

    if (options.user.emails[0].verified === true) {
      return true;
    } else {
      throw new Meteor.Error(401, 'You must verify your email address before you can log in');
    }

  });

  Accounts.onCreateUser((options, user) => {
    if (options.profile) {
      user.profile = options.profile;

      return user;
    }

    return user;
  });

  Accounts.registerLoginHandler('apple', (params) => {
    const data = params.apple;

    // If this isn't apple login then we don't care about it. No need to proceed.
    if (!data) {
      return undefined;
    }

    const clientId = 'FishPlaces';

    let userId;

    const verifyApple = Meteor.wrapAsync(async (cb) => {
      const res = await appleSignIn.verifyIdToken(
       data.identityToken,
       {
         audience: clientId
       });

      cb(null, res);
    });

    const res = verifyApple();

    if (res.sub === data.user) {
      const serviceData = {
        accessToken: data.identityToken,
        expiresAt: (+new Date) + (1000)
      };
      const fields = Object.assign({}, serviceData, res);

      // Search for an existing user with that apple id
      const existingUser = Meteor.users.findOne({
        $or: [
          { 'services.apple.sub': res.sub },
          { 'emails.0.address': res.email }
        ]
      });

      if (existingUser) {
        userId = existingUser._id;

        // Update our data to be in line with the latest from Facebook
        const prefixedData = {};
        _.each(fields, (val, key) => {
          prefixedData[`services.apple.${key}`] = val;
        });

        const modifier = {
          $set: prefixedData
        };

        if (res.email) {
          if (!existingUser.emails.length) {
            modifier.$addToSet = { emails: { address: res.email, verified: true } };
          } else {
            modifier.$set['emails.0'] = { address: res.email, verified: true };
          }
        }

        Meteor.users.update(
         { _id: userId },
         modifier
        );

      } else {
        // Create our user
        const profile = {
          language: data.language || 'en',
          lastName: data.fullName.familyName || '',
          firstName: data.fullName.givenName || ''
        };

        userId = Meteor.users.insert({
          services: {
            apple: fields
          },
          profile,
          emails: [{
            address: res.email,
            verified: true
          }]
        });

        const html = htmlTemplate('welcome', profile.language);

        const extraData = {
          name: `${profile.firstName} ${profile.lastName}`,
          language: profile.language
        };

        Email.send({
          to: `${profile.firstName} ${profile.lastName} <${res.email}>`,
          from: Meteor.settings[profile.language].email.from,
          html: html(extraData),
          subject: Meteor.settings[profile.language].email.welcome.subject
        });
      }
    }

    return { userId };
  });

  Accounts.registerLoginHandler('facebook', function (params) {
    const data = params.facebook;

    // If this isn't facebook login then we don't care about it. No need to proceed.
    if (!data) {
      return undefined;
    }


    // Get our user's identifying information. This also checks if the accessToken
    // is valid. If not it will error out.

    const fb_fields_to_get = ['id', 'email', 'first_name', 'last_name', 'picture'];

    const identity = getIdentity(data, fb_fields_to_get);

    // Build our actual data object.
    const serviceData = {
      accessToken: data.accessToken,
      expiresAt: (+new Date) + (1000 * data.expirationTime)
    };
    const fields = Object.assign({}, serviceData, identity);

    // Search for an existing user with that facebook id
    const existingUser = Meteor.users.findOne({
      $or: [
        { 'services.facebook.id': identity.id },
        { 'emails.0.address': identity.email }
      ]
    });

    let userId;
    if (existingUser) {
      userId = existingUser._id;

      // Update our data to be in line with the latest from Facebook
      const prefixedData = {};
      _.each(fields, (val, key) => {
        prefixedData[`services.facebook.${key}`] = val;
      });

      const modifier = {
        $set: prefixedData
      };

      if (!existingUser.emails.length) {
        modifier.$addToSet = { emails: { address: identity.email, verified: true } };
      } else {
        modifier.$set['emails.0'] = { address: identity.email, verified: true };
      }

      Meteor.users.update(
       { _id: userId },
       modifier
      );

    } else {
      // Create our user
      const profile = {
        language: data.language || 'en',
        lastName: identity.last_name,
        firstName: identity.first_name
      };

      userId = Meteor.users.insert({
        services: {
          facebook: fields
        },
        profile,
        emails: [{
          address: identity.email,
          verified: true
        }]
      });

      const html = htmlTemplate('welcome', profile.language);

      const extraData = {
        name: `${profile.firstName} ${profile.lastName}`,
        language: profile.language
      };

      Email.send({
        to: `${profile.firstName} ${profile.lastName} <${identity.email}>`,
        from: Meteor.settings[profile.language].email.from,
        html: html(extraData),
        subject: Meteor.settings[profile.language].email.welcome.subject
      });
    }

    return { userId: userId };
  });

  // Gets the identity of our user and by extension checks if
  // our access token is valid.
  const getIdentity = (data, fields = []) => {
    try {
      return HTTP.call(
       'GET',
       `https://graph.facebook.com/me`,
       {
         params: {
           fields: fields.join(','),
           access_token: data.accessToken
         }
       }
      ).data;
    } catch (err) {
      throw _.extend(new Error('Failed to fetch identity from Facebook. ' + err.message),
       { response: err.response });
    }
  };

  SimpleSchema.defineValidationErrorTransform(error => {
    const ddpError = new Meteor.Error(error.message);

    ddpError.error = 'validation-error';
    ddpError.reason = error.message;
    ddpError.details = error.details;

    return ddpError;
  });

  Cloudinary.config({
    api_key: Meteor.settings.cloudinary.api_key,
    api_secret: Meteor.settings.cloudinary.api_secret,
    cloud_name: Meteor.settings.cloudinary.cloud_name
  });

  // Rules are bound to the connection from which they are are executed. This means you have a userId available as this.userId if there is a logged in user. Throw a new Meteor.Error to stop the method from executing and propagate the error to the client. If rule is not set a standard error will be thrown.
  Cloudinary.rules.delete = function (publicId) {
    if (!this.userId && !publicId) throw new Meteor.Error('Not Authorized', 'Sorry, you can\'t do that!');
  };

  Cloudinary.rules.sign_upload = function () {
    if (!this.userId) throw new Meteor.Error('Not Authorized', 'Sorry, you can\'t do that!');
  };

  Cloudinary.rules.private_resource = function (publicId) {
    if (!this.userId && !publicId) throw new Meteor.Error('Not Authorized', 'Sorry, you can\'t do that!');
  };

  Cloudinary.rules.download_url = function (publicId) {
    if (!this.userId && !publicId) throw new Meteor.Error('Not Authorized', 'Sorry, you can\'t do that!');
  };

  ServiceConfiguration.configurations.remove({
    service: 'facebook'
  });

  ServiceConfiguration.configurations.insert({
    service: 'facebook',
    appId: Meteor.settings.facebook.app_id,
    secret: Meteor.settings.facebook.secret
  });

  ServiceConfiguration.configurations.remove({
    service: 'apple'
  });

  ServiceConfiguration.configurations.insert({
    service: 'apple'
  });

  firebase.initializeApp({
    credential: firebase.credential.cert(Meteor.settings.firebase)
  });

  if (Meteor.settings?.monti?.enable) {
    Monti.connect('Bax7hXWSqixge8W4M', 'b0582572-6ae1-45dd-85cc-f9329ec00f43');
  }
});
